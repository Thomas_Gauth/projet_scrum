package controller;

import model.Constantes;
import model.MainModel;
import model.deck.*;
import model.objets.Bijou;
import model.objets.Bourse;
import model.objets.Butin;
import model.objets.Malette;
import model.personnages.Personnages;
import model.train.Wagon;
import view.ButtonSound;
import view.MainWindow;
import view.deck.SlotCarte;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ControlGame extends MainControl implements ActionListener, Constantes {

    protected MainModel m;
    protected MainWindow v;
    private Timer timer, timer2;

    public ControlGame(MainModel m, MainWindow v) {
        this.m = m;
        this.v = v;
        initControl();
    }


    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == v.getBoard().getPanoDeck().getButtonDeck() && !m.getGame().isAllPioched) {
            if (m.getGame().getNumeroJoueurActuel()==3) m.getGame().isAllPioched = true;
            stopperClignoterDeck();
            m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel).piocheMain();
            mettreAjourNbCarteJoueur();
            for (int i=0; i<5; i++) {
                System.out.println(m.getGame().getNbCarteActionParJoueur()[m.getGame().getNumeroJoueurActuel()][i]);
            }
            System.out.println();
            for (int i=0; i<m.getGame().getJoueurs().get(m.getGame().getNumeroJoueurActuel()).getCartesEnMain().size(); i++) {
                System.out.println(m.getGame().getJoueurs().get(m.getGame().getNumeroJoueurActuel()).getCartesEnMain().get(i));
            }
            v.getBoard().getPanoDeck().getButtonDeck().setEnabled(false);
            if (!m.getGame().isAllPioched)
                joueurSuivantPioche();
            else
                joueurSuivant();
        }
        else if (e.getSource() == v.getBoard().getPanoRailway().getBs()){
            if (((ButtonSound)e.getSource()).isAudible()){
                ((ButtonSound)e.getSource()).setIcon(new ImageIcon("img/bouton_son_off.png"));
                ((ButtonSound)e.getSource()).setAudible(false);
              //  v.getMenu().getM().getMenu().getMusicMenu().stop();
            }
            else {
                ((ButtonSound)e.getSource()).setIcon(new ImageIcon("img/bouton_son_on.png"));
                ((ButtonSound)e.getSource()).setAudible(true);
             //   v.getMenu().getM().getMenu().getMusicMenu().play();
            }
        }
        else if (m.getGame().isLaunched && m.getGame().isManchePose && m.getGame().isAllPioched) {
            if (!m.getGame().isCarteJoue) {
                if (e.getSource() == v.getBoard().getPanoPlayerSet().getSlots()[CARTE_SHERIFF] && m.getGame().haveCarteInMain(CARTE_SHERIFF)) {
                    m.getGame().getCartesJouees().add(new CarteSheriff());
                    m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel).cartesEnMain.remove(m.getGame().indexOfSpecifiedCarteInMain(CARTE_SHERIFF));
                    m.getGame().getNbCarteActionParJoueur()[m.getGame().getNumeroJoueurActuel()][CARTE_SHERIFF]--;
                    v.getBoard().getPanoPlayerSet().getSlots()[CARTE_SHERIFF].setText("" + m.getGame().getNbCarteActionParJoueur()[m.getGame().getNumeroJoueurActuel()][CARTE_SHERIFF]);
                    v.getBoard().getPanoDeck().getPanoCarte().setIcon(((SlotCarte) e.getSource()).getIcon());

                    joueurSuivant();
                }

                if (e.getSource() == v.getBoard().getPanoPlayerSet().getSlots()[CARTE_RAMASSER] && m.getGame().haveCarteInMain(CARTE_RAMASSER)) {
                    m.getGame().getCartesJouees().add(new CarteRamasser());
                    m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel).cartesEnMain.remove(m.getGame().indexOfSpecifiedCarteInMain(CARTE_RAMASSER));
                    m.getGame().getNbCarteActionParJoueur()[m.getGame().getNumeroJoueurActuel()][CARTE_RAMASSER]--;
                    v.getBoard().getPanoDeck().getPanoCarte().setIcon(((SlotCarte) e.getSource()).getIcon());

                    // METTRE A JOUR L'AFFICHAGE DU NB DE CARTE
                    joueurSuivant();
                }

                if (e.getSource() == v.getBoard().getPanoPlayerSet().getSlots()[CARTE_MONTER] && m.getGame().haveCarteInMain(CARTE_MONTER)) {
                    m.getGame().getCartesJouees().add(new CarteMonter());
                    m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel).cartesEnMain.remove(m.getGame().indexOfSpecifiedCarteInMain(CARTE_MONTER));
                    m.getGame().getNbCarteActionParJoueur()[m.getGame().getNumeroJoueurActuel()][CARTE_MONTER]--;
                    v.getBoard().getPanoDeck().getPanoCarte().setIcon(((SlotCarte) e.getSource()).getIcon());
                    //afficherJoueurs();

                    // METTRE A JOUR L'AFFICHAGE DU NB DE CARTE
                    joueurSuivant();
                }

                if (e.getSource() == v.getBoard().getPanoPlayerSet().getSlots()[CARTE_DEPLACEMENT] && m.getGame().haveCarteInMain(CARTE_DEPLACEMENT)) {
                    m.getGame().getCartesJouees().add(new CarteDeplacement());
                    m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel).cartesEnMain.remove(m.getGame().indexOfSpecifiedCarteInMain(CARTE_DEPLACEMENT));
                    m.getGame().getNbCarteActionParJoueur()[m.getGame().getNumeroJoueurActuel()][CARTE_DEPLACEMENT]--;
                    v.getBoard().getPanoDeck().getPanoCarte().setIcon(((SlotCarte) e.getSource()).getIcon());
                    //afficherJoueurs();

                    // METTRE A JOUR L'AFFICHAGE DU NB DE CARTE
                    joueurSuivant();
                }

                if (e.getSource() == v.getBoard().getPanoPlayerSet().getSlots()[CARTE_FRAPPER] && m.getGame().haveCarteInMain(CARTE_FRAPPER)) {
                    m.getGame().getCartesJouees().add(new CarteFrapper());
                    m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel).cartesEnMain.remove(m.getGame().indexOfSpecifiedCarteInMain(CARTE_FRAPPER));
                    m.getGame().getNbCarteActionParJoueur()[m.getGame().getNumeroJoueurActuel()][CARTE_FRAPPER]--;
                    v.getBoard().getPanoDeck().getPanoCarte().setIcon(((SlotCarte) e.getSource()).getIcon());

                    // METTRE A JOUR L'AFFICHAGE DU NB DE CARTE
                    joueurSuivant();
                }
            }
        }

        //Gestion de la phase de jeu
        if (m.getGame().isLaunched && m.getGame().isMancheJeu) {
            System.out.println("YEEEEAH PHASE DE JEU");
            cacherAutres(m.getGame().getCartesJouees().get(0));
            // DEPLACEMENT DES JOUEURS//
            if (m.getGame().getCartesJouees().get(0).isDeplacement()) {
                int compteurWagon = 0;
                boolean joueurTrouve = false;
                for (Wagon w : m.getGame().train.wagons) {
                    if (!joueurTrouve) {

                        //On vérifie si le joueur est à l'intérieur de l'un des wagons.
                        if (w.getBas().contains(m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel))) {
                            joueurTrouve = true;
                            if (compteurWagon == 0) {
                                w.getBas().remove(m.getGame().joueurs.get(m.getGame().numeroJoueurActuel));
                                m.getGame().train.wagons.get(1).getBas().add(m.getGame().joueurs.get(m.getGame().numeroJoueurActuel));
                            }
                            else if (compteurWagon == NB_JOUEURS) {
                                w.getBas().remove(m.getGame().joueurs.get(m.getGame().numeroJoueurActuel));
                                m.getGame().train.wagons.get(NB_JOUEURS - 1).getBas().add(m.getGame().joueurs.get(m.getGame().numeroJoueurActuel));
                            } else {
                                v.getBoard().getPanoActions().getFlecheDroiteDeplacer().setVisible(true);
                                v.getBoard().getPanoActions().getFlecheGaucheDeplacer().setVisible(true);
                                if (e.getSource() == v.getBoard().getPanoActions().getFlecheGaucheDeplacer()) {
                                    w.getBas().remove(m.getGame().joueurs.get(m.getGame().numeroJoueurActuel));
                                    m.getGame().train.wagons.get(compteurWagon - 1).getBas().add(m.getGame().joueurs.get(m.getGame().numeroJoueurActuel));
                                }

                                if (e.getSource() == v.getBoard().getPanoActions().getFlecheDroiteDeplacer()) {
                                    w.getBas().remove(m.getGame().joueurs.get(m.getGame().numeroJoueurActuel));
                                    m.getGame().train.wagons.get(compteurWagon + 1).getBas().add(m.getGame().joueurs.get(m.getGame().numeroJoueurActuel));
                                }
                            }
                        }

                        //On vérifie si le joueur se trouve sur le toit de l'un des wagons.
                        if (w.getHaut().contains(m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel))) {
                            joueurTrouve = true;
                            if (compteurWagon == 0) {
                                w.getHaut().remove(m.getGame().joueurs.get(m.getGame().numeroJoueurActuel));
                                m.getGame().train.wagons.get(1).getHaut().add(m.getGame().joueurs.get(m.getGame().numeroJoueurActuel));
                            }
                            else if (compteurWagon == NB_JOUEURS) {
                                w.getHaut().remove(m.getGame().joueurs.get(m.getGame().numeroJoueurActuel));
                                m.getGame().train.wagons.get(NB_JOUEURS - 1).getHaut().add(m.getGame().joueurs.get(m.getGame().numeroJoueurActuel));
                            } else {
                                v.getBoard().getPanoActions().getFlecheDroiteDeplacer().setVisible(true);
                                v.getBoard().getPanoActions().getFlecheGaucheDeplacer().setVisible(true);
                                if (e.getSource() == v.getBoard().getPanoActions().getFlecheGaucheDeplacer()) {
                                    w.getHaut().remove(m.getGame().joueurs.get(m.getGame().numeroJoueurActuel));
                                    m.getGame().train.wagons.get(compteurWagon - 1).getHaut().add(m.getGame().joueurs.get(m.getGame().numeroJoueurActuel));
                                }

                                if (e.getSource() == v.getBoard().getPanoActions().getFlecheDroiteDeplacer()) {
                                    w.getHaut().remove(m.getGame().joueurs.get(m.getGame().numeroJoueurActuel));
                                    m.getGame().train.wagons.get(compteurWagon + 1).getHaut().add(m.getGame().joueurs.get(m.getGame().numeroJoueurActuel));

                                }
                            }
                        }
                        compteurWagon ++;
                    }
                }
                joueurSuivant();
            }

            //DEPLACEMENT DU SHERIFF//
            if (m.getGame().getCartesJouees().get(0).isSheriff()) {
                int compteurWagon = 0;
                boolean sheriffTrouve = false;
                for (Wagon w : m.getGame().train.wagons) {
                    if (!sheriffTrouve) {
                        if (w.getBas().contains(m.getGame().getSheriff())) {
                            sheriffTrouve = true;
                            if (compteurWagon == 0) {
                                w.getBas().remove(m.getGame().getSheriff());
                                m.getGame().train.wagons.get(1).getBas().add(m.getGame().getSheriff());
                            }
                            else if (compteurWagon == NB_JOUEURS) {
                                w.getBas().remove(m.getGame().getSheriff());
                                m.getGame().train.wagons.get(NB_JOUEURS - 1).getBas().add(m.getGame().getSheriff());
                            } else {
                                v.getBoard().getPanoActions().getFlecheDroiteDeplacer().setVisible(true);
                                v.getBoard().getPanoActions().getFlecheGaucheDeplacer().setVisible(true);

                                if (e.getSource() == v.getBoard().getPanoActions().getFlecheGaucheDeplacer()) {
                                    w.getBas().remove(m.getGame().getSheriff());
                                    m.getGame().train.wagons.get(compteurWagon - 1).getBas().add(m.getGame().getSheriff());
                                }

                                if (e.getSource() == v.getBoard().getPanoActions().getFlecheDroiteDeplacer()) {
                                    w.getBas().remove(m.getGame().getSheriff());
                                    m.getGame().train.wagons.get(compteurWagon + 1).getBas().add(m.getGame().getSheriff());
                                }
                            }
                        }
                        compteurWagon ++;
                    }
                }
                joueurSuivant();
            }

            //FRAPPER UN ADVERSAIRE//
            if (m.getGame().getCartesJouees().get(0).isFrapper()) {
                boolean joueurTrouve = false;
                for (Wagon w : m.getGame().train.wagons) {
                    if (!joueurTrouve) {

                        //On vérifie si le joueur est à l'intérieur de l'un des wagons.
                        if (w.getBas().contains(m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel))) {
                            joueurTrouve = true;
                            for (Personnages p : w.getBas()) {
                                if (!p.equals(m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel))) {
                                    if (p.equals(m.getGame().getJoueurs().get(0))) {
                                        v.getBoard().getPanoActions().getPersonnagesFrapper()[1].setVisible(true);
                                    }
                                    if (p.equals(m.getGame().getJoueurs().get(1))) {
                                        v.getBoard().getPanoActions().getPersonnagesFrapper()[2].setVisible(true);
                                    }
                                    if (p.equals(m.getGame().getJoueurs().get(2))) {
                                        v.getBoard().getPanoActions().getPersonnagesFrapper()[3].setVisible(true);
                                    }
                                    if (p.equals(m.getGame().getJoueurs().get(3))) {
                                        v.getBoard().getPanoActions().getPersonnagesFrapper()[4].setVisible(true);
                                    }
                                }
                            }
                        }
                    }
                }
                if (e.getSource() == v.getBoard().getPanoActions().getPersonnagesFrapper()[1]) {
                    for (int i = 1; i <= 4; i++) {
                        v.getBoard().getPanoPlayerSet().getSlots()[i].setVisible(false);
                    }
                    boolean butinPossede = false;
                    if (m.getGame().getJoueurs().get(0).getTresor().contains(Bijou.class)) {
                        butinPossede = true;
                        //TODO : setVisible bouton pour selectionner un bijou
                    }

                    if (m.getGame().getJoueurs().get(0).getTresor().contains(Bourse.class)) {
                        butinPossede = true;
                       //TODO : setVisible bouton pour selectionner une bourse
                    }

                    if (m.getGame().getJoueurs().get(0).getTresor().contains(Malette.class)) {
                        butinPossede = true;
                        //TODO : setVisible bouton pour selectionner une malette
                    }

                    if (butinPossede) {
                        if (true/*Todo e.getSource() ==   boutonBijou*/) {
                            for (Butin b : m.getGame().getJoueurs().get(0).getTresor()) {
                                if (b.equals(Bijou.class)) {
                                    m.getGame().getJoueurs().get(0).getTresor().remove(b);
                                    m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel).getTresor().add(b);
                                    break;
                                }
                            }
                        }

                        if (true /*Todo e.getSource() == boutonBourse*/) {
                            for (Butin b : m.getGame().getJoueurs().get(0).getTresor()) {
                                if (b.equals(Bourse.class)) {
                                    m.getGame().getJoueurs().get(0).getTresor().remove(b);
                                    m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel).getTresor().add(b);
                                    break;
                                }
                            }
                        }

                        if (true /* Todo : e.getSource() == boutonMalette*/) {
                            for (Butin b : m.getGame().getJoueurs().get(0).getTresor()) {
                                if (b.equals(Malette.class)) {
                                    m.getGame().getJoueurs().get(0).getTresor().remove(b);
                                    m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel).getTresor().add(b);
                                    break;
                                }
                            }
                        }
                        //Todo : Set visible false boutons de butin
                    }
                }

                if (e.getSource() == v.getBoard().getPanoPlayerSet().getSlots()[2]) {
                    for (int i = 1; i <= 4; i++) {
                        v.getBoard().getPanoPlayerSet().getSlots()[i].setVisible(false);
                    }
                    boolean butinPossede = false;
                    if (m.getGame().getJoueurs().get(1).getTresor().contains(Bijou.class)) {
                        butinPossede = true;
                        //TODO : setVisible bouton pour selectionner un bijou
                    }

                    if (m.getGame().getJoueurs().get(1).getTresor().contains(Bourse.class)) {
                        butinPossede = true;
                        //TODO : setVisible bouton pour selectionner une bourse
                    }

                    if (m.getGame().getJoueurs().get(1).getTresor().contains(Malette.class)) {
                        butinPossede = true;
                        //TODO : setVisible bouton pour selectionner une malette
                    }

                    if (butinPossede) {
                        if (true /*Todo : e.getSource() == boutonBijou*/) {
                            for (Butin b : m.getGame().getJoueurs().get(1).getTresor()) {
                                if (b.equals(Bijou.class)) {
                                    m.getGame().getJoueurs().get(1).getTresor().remove(b);
                                    m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel).getTresor().add(b);
                                    break;
                                }
                            }
                        }

                        if (true /*Todo : e.getSource() == boutonBourse*/) {
                            for (Butin b : m.getGame().getJoueurs().get(1).getTresor()) {
                                if (b.equals(Bourse.class)) {
                                    m.getGame().getJoueurs().get(1).getTresor().remove(b);
                                    m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel).getTresor().add(b);
                                    break;
                                }
                            }
                        }

                        if (true /*Todo : e.getSource() == boutonMalette*/) {
                            for (Butin b : m.getGame().getJoueurs().get(1).getTresor()) {
                                if (b.equals(Malette.class)) {
                                    m.getGame().getJoueurs().get(1).getTresor().remove(b);
                                    m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel).getTresor().add(b);
                                    break;
                                }
                            }
                        }
                        //Todo : Set visible false boutons de butin
                    }
                }

                if (e.getSource() == v.getBoard().getPanoPlayerSet().getSlots()[3]) {
                    for (int i = 1; i <= 4; i++) {
                        v.getBoard().getPanoPlayerSet().getSlots()[i].setVisible(false);
                    }
                    boolean butinPossede = false;
                    if (m.getGame().getJoueurs().get(2).getTresor().contains(Bijou.class)) {
                        butinPossede = true;
                        //TODO : setVisible bouton pour selectionner un bijou
                    }

                    if (m.getGame().getJoueurs().get(2).getTresor().contains(Bourse.class)) {
                        butinPossede = true;
                        //TODO : setVisible bouton pour selectionner une bourse
                    }

                    if (m.getGame().getJoueurs().get(2).getTresor().contains(Malette.class)) {
                        butinPossede = true;
                        //TODO : setVisible bouton pour selectionner une malette
                    }

                    if (butinPossede) {
                        if (true /*Todo : e.getSource() == boutonBijou*/) {
                            for (Butin b : m.getGame().getJoueurs().get(2).getTresor()) {
                                if (b.equals(Bijou.class)) {
                                    m.getGame().getJoueurs().get(2).getTresor().remove(b);
                                    m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel).getTresor().add(b);
                                    break;
                                }
                            }
                        }

                        if (true /*Todo : e.getSource() == boutonBourse*/) {
                            for (Butin b : m.getGame().getJoueurs().get(2).getTresor()) {
                                if (b.equals(Bourse.class)) {
                                    m.getGame().getJoueurs().get(2).getTresor().remove(b);
                                    m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel).getTresor().add(b);
                                    break;
                                }
                            }
                        }

                        if (true /*Todo : e.getSource() == boutonMalette*/) {
                            for (Butin b : m.getGame().getJoueurs().get(2).getTresor()) {
                                if (b.equals(Malette.class)) {
                                    m.getGame().getJoueurs().get(2).getTresor().remove(b);
                                    m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel).getTresor().add(b);
                                    break;
                                }
                            }
                        }
                        //Todo : Set visible false boutons de butin
                    }
                }

                if (e.getSource() == v.getBoard().getPanoPlayerSet().getSlots()[3]) {
                    for (int i = 1; i <= 4; i++) {
                        v.getBoard().getPanoPlayerSet().getSlots()[i].setVisible(false);
                    }
                    boolean butinPossede = false;
                    if (m.getGame().getJoueurs().get(3).getTresor().contains(Bijou.class)) {
                        butinPossede = true;
                        //TODO : setVisible bouton pour selectionner un bijou
                    }

                    if (m.getGame().getJoueurs().get(3).getTresor().contains(Bourse.class)) {
                        butinPossede = true;
                        //TODO : setVisible bouton pour selectionner une bourse
                    }

                    if (m.getGame().getJoueurs().get(3).getTresor().contains(Malette.class)) {
                        butinPossede = true;
                        //TODO : setVisible bouton pour selectionner une malette
                    }

                    if (butinPossede) {
                        if (true /*Todo : e.getSource() == boutonBijou*/) {
                            for (Butin b : m.getGame().getJoueurs().get(3).getTresor()) {
                                if (b.equals(Bijou.class)) {
                                    m.getGame().getJoueurs().get(3).getTresor().remove(b);
                                    m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel).getTresor().add(b);
                                    break;
                                }
                            }
                        }

                        if (true /*Todo : e.getSource() == boutonBourse*/) {
                            for (Butin b : m.getGame().getJoueurs().get(3).getTresor()) {
                                if (b.equals(Bourse.class)) {
                                    m.getGame().getJoueurs().get(3).getTresor().remove(b);
                                    m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel).getTresor().add(b);
                                    break;
                                }
                            }
                        }

                        if (true /*Todo : e.getSource() == boutonMalette*/) {
                            for (Butin b : m.getGame().getJoueurs().get(3).getTresor()) {
                                if (b.equals(Malette.class)) {
                                    m.getGame().getJoueurs().get(3).getTresor().remove(b);
                                    m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel).getTresor().add(b);
                                    break;
                                }
                            }
                        }
                    }
                }
                joueurSuivant();
            }

            //MONTER OU DESCENDRE//
            if (m.getGame().getCartesJouees().get(0).isMonter()) {
                boolean joueurTrouve = false;
                for (Wagon w : m.getGame().train.wagons) {
                    if (!joueurTrouve) {

                        //On vérifie si le joueur est à l'intérieur de l'un des wagons.
                        if (w.getBas().contains(m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel))) {
                            joueurTrouve = true;
                            w.getBas().remove(m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel));
                            w.getHaut().add(m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel));
                        }

                        //On vérifie si le joueur est sur le toit de l'un des wagons des wagons.
                        if (w.getHaut().contains(m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel))) {
                            joueurTrouve = true;
                            w.getHaut().remove(m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel));
                            w.getBas().add(m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel));
                        }
                    }
                }
            }

            //RAMASSER UN BUTIN//
            if (m.getGame().getCartesJouees().get(0).isRamasser()) {
                boolean joueurTrouve = false;
                for (Wagon w : m.getGame().train.wagons) {
                    if (!joueurTrouve) {

                        //Le joueur ne peut récupérer un butin que s'il n'est pas sur le toit
                        if (w.getBas().contains(m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel))) {
                            if (!w.getButin().isEmpty()) {
                                if (w.getButin().contains(Bourse.class)) {
                                    //TODO : set visible bouton bourse
                                }
                                if (w.getButin().contains(Bijou.class)) {
                                    //TODO : set visible bouton bijou
                                }
                                if (w.getButin().contains(Malette.class)) {
                                    //TODO : set visible bouton malette
                                }

                                if (true /*Todo : e.getSource() == bouton Bourse*/) {
                                    for (Butin b : w.getButin()) {
                                        if(b.equals(Bourse.class)) {
                                            w.getButin().remove(b);
                                            m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel).getTresor().add(b);
                                        }
                                    }
                                }

                                if (true /*Todo : e.getSource() == bouton bijou*/) {
                                    for (Butin b : w.getButin()) {
                                        if(b.equals(Bijou.class)) {
                                            w.getButin().remove(b);
                                            m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel).getTresor().add(b);
                                        }
                                    }
                                }

                                if (true /*Todo : e.getSource() == bouton Malette*/) {
                                    for (Butin b : w.getButin()) {
                                        if(b.equals(Malette.class)) {
                                            w.getButin().remove(b);
                                            m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel).getTresor().add(b);
                                        }
                                    }
                                }
                                //Todo : rendre les boutons de butin invisibles
                            }
                        }
                    }
                }
                m.getGame().getCartesJouees().remove(0);
                joueurSuivant();
            }
            actualiserDeck(true);
        }
    }

    private void actualiserDeck(boolean effacerCarte) {
        if (effacerCarte){
            m.getGame().getCartesJouees().remove(0);
        }
        if (m.getGame().getCartesJouees().get(0).isDeplacement()){
            v.getBoard().getPanoDeck().getPanoCarte().setIcon(new ImageIcon("iconeCarteDeplacement"));
        } else if (m.getGame().getCartesJouees().get(0).isSheriff()){
            v.getBoard().getPanoDeck().getPanoCarte().setIcon(new ImageIcon("iconeCarteSheriff"));
        } else if (m.getGame().getCartesJouees().get(0).isRamasser()){
            v.getBoard().getPanoDeck().getPanoCarte().setIcon(new ImageIcon("iconeCarteRamasser"));
        } else if (m.getGame().getCartesJouees().get(0).isFrapper()){
            v.getBoard().getPanoDeck().getPanoCarte().setIcon(new ImageIcon("iconeCarteFrapper"));
        } else if (m.getGame().getCartesJouees().get(0).isMonter()){
            v.getBoard().getPanoDeck().getPanoCarte().setIcon(new ImageIcon("iconeCarteMonter"));
        }
    }

    private void cacherAutres(Cartes carte) {
        v.getBoard().getPanoActions().getBoutonEchelle().setVisible(true);
        v.getBoard().getPanoActions().getBijou().setVisible(true);
        v.getBoard().getPanoActions().getMalette().setVisible(true);
        v.getBoard().getPanoActions().getBourse().setVisible(true);
        v.getBoard().getPanoActions().getFlecheDroiteDeplacer().setVisible(true);
        v.getBoard().getPanoActions().getFlecheGaucheDeplacer().setVisible(true);

        for (int i=0; i <v.getBoard().getPanoActions().getPersonnagesFrapper().length; i++){
            v.getBoard().getPanoActions().getPersonnagesFrapper()[i].setVisible(true);
        }

        if (carte.isDeplacement() || carte.isSheriff()){
            System.out.println("JE CACHE CAR CARTE DEPLACEMENT OU SHERIFF");
            v.getBoard().getPanoActions().getBoutonEchelle().setVisible(false);
            v.getBoard().getPanoActions().getBijou().setVisible(false);
            v.getBoard().getPanoActions().getMalette().setVisible(false);
            v.getBoard().getPanoActions().getBourse().setVisible(false);
            for (int i=0; i <v.getBoard().getPanoActions().getPersonnagesFrapper().length; i++){
                v.getBoard().getPanoActions().getPersonnagesFrapper()[i].setVisible(false);
            }
        }
        else if (carte.isFrapper()){
            System.out.println("JE CACHE CAR CARTE FRAPPER");

            v.getBoard().getPanoActions().getBoutonEchelle().setVisible(false);
            v.getBoard().getPanoActions().getBijou().setVisible(false);
            v.getBoard().getPanoActions().getMalette().setVisible(false);
            v.getBoard().getPanoActions().getBourse().setVisible(false);
            v.getBoard().getPanoActions().getFlecheDroiteDeplacer().setVisible(false);
            v.getBoard().getPanoActions().getFlecheGaucheDeplacer().setVisible(false);
        }
        else if (carte.isMonter()){
            System.out.println("JE CACHE CAR CARTE MONTER");

            v.getBoard().getPanoActions().getBijou().setVisible(false);
            v.getBoard().getPanoActions().getMalette().setVisible(false);
            v.getBoard().getPanoActions().getBourse().setVisible(false);
            v.getBoard().getPanoActions().getFlecheDroiteDeplacer().setVisible(false);
            v.getBoard().getPanoActions().getFlecheGaucheDeplacer().setVisible(false);
            for (int i=0; i <v.getBoard().getPanoActions().getPersonnagesFrapper().length; i++){
                v.getBoard().getPanoActions().getPersonnagesFrapper()[i].setVisible(false);
            }
        }
        else if (carte.isRamasser()){
            System.out.println("JE CACHE CAR CARTE RAMASSER");

            v.getBoard().getPanoActions().getBoutonEchelle().setVisible(false);
            v.getBoard().getPanoActions().getFlecheDroiteDeplacer().setVisible(false);
            v.getBoard().getPanoActions().getFlecheGaucheDeplacer().setVisible(false);
            for (int i=0; i <v.getBoard().getPanoActions().getPersonnagesFrapper().length; i++){
                v.getBoard().getPanoActions().getPersonnagesFrapper()[i].setVisible(false);
            }
        }
    }


    public void mettreAjourNbCarteJoueur() {
        if (!m.getGame().isCarteJoue) {
            m.getGame().initNbCarteJoueur();
            for (int i = 0; i < m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel).getCartesEnMain().size(); i++) {
                if (m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel).getCartesEnMain().get(i).isSheriff())
                    m.getGame().getNbCarteActionParJoueur()[m.getGame().getNumeroJoueurActuel()][CARTE_SHERIFF]++;
                else if (m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel).getCartesEnMain().get(i).isRamasser())
                    m.getGame().getNbCarteActionParJoueur()[m.getGame().getNumeroJoueurActuel()][CARTE_RAMASSER]++;
                else if (m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel).getCartesEnMain().get(i).isMonter())
                    m.getGame().getNbCarteActionParJoueur()[m.getGame().getNumeroJoueurActuel()][CARTE_MONTER]++;
                else if (m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel).getCartesEnMain().get(i).isDeplacement())
                    m.getGame().getNbCarteActionParJoueur()[m.getGame().getNumeroJoueurActuel()][CARTE_DEPLACEMENT]++;
                else if (m.getGame().getJoueurs().get(m.getGame().numeroJoueurActuel).getCartesEnMain().get(i).isFrapper())
                    m.getGame().getNbCarteActionParJoueur()[m.getGame().getNumeroJoueurActuel()][CARTE_FRAPPER]++;
            }
        }

        if (m.getGame().getNbCarteActionParJoueur()[m.getGame().getNumeroJoueurActuel()][CARTE_SHERIFF]==0) {
            v.getBoard().getPanoPlayerSet().getSlots()[CARTE_SHERIFF].setEnabled(false);
            v.getBoard().getPanoPlayerSet().getSlots()[CARTE_SHERIFF].setText(null);
        }
        else {
            v.getBoard().getPanoPlayerSet().getSlots()[CARTE_SHERIFF].setEnabled(true);
            v.getBoard().getPanoPlayerSet().getSlots()[CARTE_SHERIFF].setText("" + m.getGame().getNbCarteActionParJoueur()[m.getGame().getNumeroJoueurActuel()][CARTE_SHERIFF]);
        }


        if (m.getGame().getNbCarteActionParJoueur()[m.getGame().getNumeroJoueurActuel()][CARTE_RAMASSER]==0) {
            v.getBoard().getPanoPlayerSet().getSlots()[CARTE_RAMASSER].setEnabled(false);
            v.getBoard().getPanoPlayerSet().getSlots()[CARTE_RAMASSER].setText(null);
        }
        else {
            v.getBoard().getPanoPlayerSet().getSlots()[CARTE_RAMASSER].setEnabled(true);
            v.getBoard().getPanoPlayerSet().getSlots()[CARTE_RAMASSER].setText("" + m.getGame().getNbCarteActionParJoueur()[m.getGame().getNumeroJoueurActuel()][CARTE_RAMASSER]);
        }

        if (m.getGame().getNbCarteActionParJoueur()[m.getGame().getNumeroJoueurActuel()][CARTE_MONTER]==0) {
            v.getBoard().getPanoPlayerSet().getSlots()[CARTE_MONTER].setEnabled(false);
            v.getBoard().getPanoPlayerSet().getSlots()[CARTE_MONTER].setText(null);
        }
        else {
            v.getBoard().getPanoPlayerSet().getSlots()[CARTE_MONTER].setEnabled(true);
            v.getBoard().getPanoPlayerSet().getSlots()[CARTE_MONTER].setText("" + m.getGame().getNbCarteActionParJoueur()[m.getGame().getNumeroJoueurActuel()][CARTE_MONTER]);
        }

        if (m.getGame().getNbCarteActionParJoueur()[m.getGame().getNumeroJoueurActuel()][CARTE_DEPLACEMENT]==0) {
            v.getBoard().getPanoPlayerSet().getSlots()[CARTE_DEPLACEMENT].setEnabled(false);
            v.getBoard().getPanoPlayerSet().getSlots()[CARTE_DEPLACEMENT].setText(null);
        }
        else {
            v.getBoard().getPanoPlayerSet().getSlots()[CARTE_DEPLACEMENT].setEnabled(true);
            v.getBoard().getPanoPlayerSet().getSlots()[CARTE_DEPLACEMENT].setText("" + m.getGame().getNbCarteActionParJoueur()[m.getGame().getNumeroJoueurActuel()][CARTE_DEPLACEMENT]);
        }

        if (m.getGame().getNbCarteActionParJoueur()[m.getGame().getNumeroJoueurActuel()][CARTE_FRAPPER]==0) {
            v.getBoard().getPanoPlayerSet().getSlots()[CARTE_FRAPPER].setEnabled(false);
            v.getBoard().getPanoPlayerSet().getSlots()[CARTE_FRAPPER].setText(null);
        }
        else {
            v.getBoard().getPanoPlayerSet().getSlots()[CARTE_FRAPPER].setEnabled(true);
            v.getBoard().getPanoPlayerSet().getSlots()[CARTE_FRAPPER].setText("" + m.getGame().getNbCarteActionParJoueur()[m.getGame().getNumeroJoueurActuel()][CARTE_FRAPPER]);
        }
    }

    public void initControl() {
        v.getBoard().getPanoDeck().getButtonDeck().addActionListener(this);
        v.getBoard().getPanoRailway().getBs().addActionListener(this);
        v.getBoard().getPanoActions().getFlecheDroiteDeplacer().addActionListener(this);
        v.getBoard().getPanoActions().getBoutonEchelle().addActionListener(this);
        v.getBoard().getPanoActions().getFlecheGaucheDeplacer().addActionListener(this);

        for(int i=0; i<5; i++) {
            v.getBoard().getPanoPlayerSet().getSlots()[i].addActionListener(this);
            if (i<4){
                v.getBoard().getPanoActions().getPersonnagesFrapper()[i].addActionListener(this);
            }
        }
        afficherJoueurs();
    }

    public void joueurSuivant() {
        timer = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Color lastColor = m.getGame().getCouleurActuel();
                System.out.println("Tour actuel : " + m.getGame().numeroTour);
                if (m.getGame().numeroTour==3 && m.getGame().getNumeroJoueurActuel()==3) {
                    m.getGame().joueurSuivant();
                    actualiserDeck(false);
                    v.getBoard().getPanoRailway().setNumeroTour();
                    m.getGame().isCarteJoue=true;
                    v.getBoard().changePanoToAction();
                    cacherAutres(m.getGame().getCartesJouees().get(0));
                    System.out.println("DERNIERE CARTE : "+m.getGame().getCartesJouees().get(0));
                }
                else {
                    m.getGame().joueurSuivant();
                    v.getBoard().getPanoRailway().setNumeroTour();
                    System.out.println(m.getGame().getNumeroJoueurActuel());
                    mettreAjourNbCarteJoueur();
                    v.getBoard().getPanoPlayerSet().changeCarteColor(lastColor, m.getGame().getCouleurActuel());
                }
            }
        });
        timer.setRepeats(false);
        timer.start();
        // updateAffichageNbCarte();
    }

    public void afficherJoueurs(){
        for (int i=0;i<m.getGame().getJoueurs().size();i++) {
            v.getBoard().getPanoRailway().getWagons()[m.getGame().getJoueurs().get(i).getNumeroWagon()].ajouterJoueur(m.getGame().getJoueurs().get(i));
        }
    }

    private void joueurSuivantPioche() {
        timer = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Color lastColor = m.getGame().getCouleurActuel();
                m.getGame().joueurSuivant();
                clignoterDeck();
                System.out.println(m.getGame().getNumeroJoueurActuel());
                mettreAjourNbCarteJoueur();
                v.getBoard().getPanoDeck().getButtonDeck().setEnabled(true);
                v.getBoard().getPanoPlayerSet().changeCarteColor(lastColor, m.getGame().getCouleurActuel());
                if (m.getGame().isAllPioched) {
                    stopperClignoterDeck();
                    v.getBoard().getPanoDeck().getButtonDeck().setEnabled(false);
                }


            }
        });
        timer.setRepeats(false);
        timer.start();
    }


    public void clignoterDeck() {
        timer2 = new Timer(700, new ActionListener() {
            boolean green = true;
            @Override

            public void actionPerformed(ActionEvent e) {
                if (green) {
                    v.getBoard().getPanoDeck().getButtonDeck().setBorder(BorderFactory.createLineBorder(Color.decode("#000000"), 4));
                    green = false;
                }
                else{
                    v.getBoard().getPanoDeck().getButtonDeck().setBorder(BorderFactory.createLineBorder(Color.decode("#ffffff"), 3));
                    green = true;
                }
            }

        });

        timer2.start();
    }

    public void stopperClignoterDeck() {
        v.getBoard().getPanoDeck().getButtonDeck().setBorder(null);
        timer2.stop();
    }
}
