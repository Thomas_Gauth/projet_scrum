package controller;

import model.Constantes;
import model.MainModel;
import model.personnages.Joueur;
import view.MainWindow;
import view.Menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

public class ControlMenu extends MainControl implements ActionListener {

    protected MainModel m;
    protected MainWindow v;
    protected ControlGame cg;

    public ControlMenu(MainModel m, MainWindow v, ControlGame cg) {
        this.m = m;
        this.v = v;
        this.cg = cg;
        this.v.getMenu().setControlMenu(this);
    }



   public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.v.getMenu().getjBNouvellePartie()) {
            lancePartie();
        }
        else if (e.getSource() == this.v.getMenu().getjBAfficherRegles())
        {
            this.v.getMenu().repaint();
            try {
                java.awt.Desktop.getDesktop().open(new File("rulesFR.pdf"));
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            this.v.getMenu().setVisible(true);
        }
        else if (e.getSource() == this.v.getMenu().getjBQuitter())
        {
            System.exit(0);
        }
   }

    public void lancePartie() {
        this.v.getMenu().setVisible(false);
        this.v.getBoard().setVisible(true);
        m.getGame().isLaunched = true;
        m.getGame().isManchePose = true;
        for (Joueur j : m.getGame().getJoueurs()) {
                j.initCartes();
        }
        cg.clignoterDeck();
    }
}