package controller;

import model.MainModel;
import model.PanelModel;
import view.Board;
import view.MainWindow;

public abstract class MainControl {
    protected static MainWindow actualView;
    protected static MainModel actualModel;
    protected static ControlGame cg;
    protected static ControlMenu cm;

    public static void initControl(MainModel model) {
        actualModel = model;
        actualView = new MainWindow(model);
        cg = new ControlGame(actualModel, actualView);
        cm = new ControlMenu(actualModel, actualView, cg); // menu a besoin du controlGame pour appeler la methode jouerPartie() lors du clique sur play
        //actualWindow = new Board(model);
    }
}
