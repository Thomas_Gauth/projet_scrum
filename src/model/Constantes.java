package model;

import java.awt.*;

public interface Constantes {
    public static final byte NB_JOUEURS = 4;
    public static final byte NB_TOURS = 4;
    public static final byte NB_MANCHES = 5; // laisser 5 manches c'est dans les règles
    public static final byte NB_CARTES_EN_MAIN = 6;
    public static final int CARTE_SHERIFF = 0;
    public static final int CARTE_RAMASSER = 1;
    public static final int CARTE_MONTER = 2;
    public static final int CARTE_DEPLACEMENT = 3;
    public static final int CARTE_FRAPPER = 4;

    public static final Color COULEUR_JOUEUR1 = Color.decode("#1001d4");
    public static final Color COULEUR_JOUEUR2 = Color.decode("#e30101");
    public static final Color COULEUR_JOUEUR3 = Color.decode("#01b127");
    public static final Color COULEUR_JOUEUR4 = Color.decode("#ffec00");


}
