package model;

import model.deck.*;
import model.personnages.Joueur;
import model.personnages.Sheriff;
import model.train.Train;

import java.awt.*;
import java.util.ArrayList;

public class GameModel implements Constantes {
    public final ArrayList<Joueur> joueurs;
    private ArrayList<Cartes> cartesJouees;
    private int[][] nbCarteActionParJoueur;
    private Sheriff sheriff;

    public Train train;
    public int numeroJoueurActuel;
    public int numeroManche, numeroTour;
    public boolean isLaunched, isManchePose, isMancheJeu, isAllPioched, isCarteJoue;

    public GameModel() {
        joueurs = new ArrayList<Joueur>(NB_JOUEURS);
        cartesJouees = new ArrayList<Cartes>();
        nbCarteActionParJoueur = new int[4][5]; // 4 joueurs, 5 carte action
        initPersonnages(NB_JOUEURS);
        initTrain();
        initNbCarteJoueur();
        isLaunched = false;
        isAllPioched = false;
        isCarteJoue = false;
        numeroJoueurActuel = 0;
        numeroTour = 0;
        numeroManche = 1;
    }

    public void initNbCarteJoueur() {
        for (int i=0; i<4; i++) {
            for (int j=0; j<5; j++) {
                nbCarteActionParJoueur[i][j] = 0;
            }
        }
    }

    public void initPersonnages(int nbJoueurs) {
        sheriff = new Sheriff();
        for (int i = 0; i < nbJoueurs; i++) {
            joueurs.add(new Joueur(i));
        }
    }

    public void initTrain() {
        train = new Train(NB_JOUEURS);
        initWagons();
    }

    public void initWagons() {
        train.wagons.get(0).getBas().add(sheriff);
        for (Joueur j : joueurs) {
            if (j.numeroPersonnage%2 == 0) {
                train.wagons.get(NB_JOUEURS - 1).getBas().add(j);
            } else {
                train.wagons.get(NB_JOUEURS).getBas().add(j);
            }
        }
    }

    public Color getCouleurActuel() {
        Color retour = Color.BLACK;
        for (int i=0; i<4; i++) {
            if (joueurs.get(i).getNumeroPersonnage()==numeroJoueurActuel)
                retour = joueurs.get(i).getCouleur();
        }
        return retour;
    }


    public void joueurSuivant() {
        if (numeroJoueurActuel == 3) {
            numeroJoueurActuel = 0;
            if (isAllPioched)
                numeroTour ++;
        } else {
            numeroJoueurActuel ++;
        }
        if (numeroTour == NB_TOURS) {
            System.out.println("NBR DE TOURS ATTEINT");
            isCarteJoue = true;
            numeroTour = 1;
            if (isManchePose) {
                isManchePose = false;
                isMancheJeu = true;
            } else if (isMancheJeu) {
                numeroManche++;
                isMancheJeu = false;
                if (numeroManche <= NB_MANCHES) {
                    isManchePose = true;
                } else {
                    isLaunched = false;
                }
            }
        }
    }

    public boolean haveCarteInMain(int indexCarte) {
        return nbCarteActionParJoueur[numeroJoueurActuel][indexCarte]>0;
    }

    public int indexOfSpecifiedCarteInMain(int indexCarte) {
        int retour = -1;
        for (int i=0; i<joueurs.get(numeroJoueurActuel).cartesEnMain.size(); i++) {
            if (indexCarte==0) {
                if (joueurs.get(numeroJoueurActuel).cartesEnMain.get(i).isSheriff())
                    retour = i;
            }
            else if (indexCarte==1) {
                if (joueurs.get(numeroJoueurActuel).cartesEnMain.get(i).isRamasser())
                    retour = i;
            }
            else if (indexCarte==2) {
                if (joueurs.get(numeroJoueurActuel).cartesEnMain.get(i).isMonter())
                    retour = i;
            }
            else if (indexCarte==3) {
                if (joueurs.get(numeroJoueurActuel).cartesEnMain.get(i).isDeplacement())
                    retour = i;
            }
            else if (indexCarte==4) {
                if (joueurs.get(numeroJoueurActuel).cartesEnMain.get(i).isFrapper())
                    retour = i;
            }
        }
        return retour;
    }

    public int getNumeroJoueurActuel() {
        return numeroJoueurActuel;
    }

    public Sheriff getSheriff() {
        return sheriff;
    }

    public ArrayList<Joueur> getJoueurs() {
        return joueurs;
    }

    public ArrayList<Cartes> getCartesJouees() {
        return cartesJouees;
    }

    public int[][] getNbCarteActionParJoueur() {
        return nbCarteActionParJoueur;
    }
}
