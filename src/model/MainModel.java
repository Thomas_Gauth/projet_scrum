package model;

public class MainModel implements Constantes {
    private GameModel game;
    private MenuModel menu;
    private PanelModel panel;

    public MainModel() {
        game = new GameModel();
        panel = new PanelModel();
        menu = new MenuModel();
    }

    public void newGame() {
        game = new GameModel();
    }

    public GameModel getGame() {
        return game;
    }

    public MenuModel getMenu() {
        return menu;
    }

    public PanelModel getPanel() {
        return panel;
    }
}
