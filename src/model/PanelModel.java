package model;

import java.awt.Dimension;
import java.awt.Toolkit;

public class PanelModel {
	
	private int widthMenu;
	private int heightMenu;
	
	private int widthBoard;
	private int heightBoard;
	
	private int widthDeck;
	private int heightDeck;

    private int widthButton;
	private int heightButton;

	private int posXButtonPlay;
	private int posYButtonPlay;

	private int posXButtonRules;
	private int posYButtonRules;

	private int posXButtonQuit;
	private int posYButtonQuit;
	
	Dimension dim;

	
	public PanelModel() {
		dim = Toolkit.getDefaultToolkit().getScreenSize();
		
		widthMenu = 1004;
		heightMenu = 343;
		
		//this.widthBoard = (int) dim.getSize().getWidth();
		//this.heightBoard = (int) dim.getSize().getHeight()/5*2;
		widthBoard = 960;
		heightBoard = 500;

		widthDeck = this.widthBoard/4;
		heightDeck = this.heightBoard;

		widthButton = 250;
		heightButton = 80;

		posXButtonPlay = (widthMenu/2)-(widthButton/2);
		posYButtonPlay = 60;

		posXButtonRules = (widthMenu/2)-(widthButton/2);
		posYButtonRules = 120;

		posXButtonQuit = (widthMenu/2)-(widthButton/2);
		posYButtonQuit = 180;
	}

	public int getWidthMenu() {
		return widthMenu;
	}

	public void setWidthMenu(int widthMenu) {
		this.widthMenu = widthMenu;
	}

	public int getHeightMenu() {
		return heightMenu;
	}

	public void setHeightMenu(int heightMenu) {
		this.heightMenu = heightMenu;
	}

	public int getWidthBoard() {
		return widthBoard;
	}

	public void setWidthBoard(int widthBoard) {
		this.widthBoard = widthBoard;
	}

	public int getHeightBoard() {
		return heightBoard;
	}

	public void setHeightBoard(int heightBoard) {
		this.heightBoard = heightBoard;
	}

	public int getWidthDeck() {
		return widthDeck;
	}

	public void setWidthDeck(int widthDeck) {
		this.widthDeck = widthDeck;
	}

	public int getHeightDeck() {
		return heightDeck;
	}

	public void setHeightDeck(int heightDeck) {
		this.heightDeck = heightDeck;
	}

	public Dimension getDim() {
		return dim;
	}

	public void setDim(Dimension dim) {
		this.dim = dim;
	}
    public int getWidthButton() {
        return widthButton;
    }

    public int getHeightButton() {
        return heightButton;
    }

    public int getPosXButtonPlay() {
        return posXButtonPlay;
    }

    public int getPosYButtonPlay() {
        return posYButtonPlay;
    }

    public int getPosXButtonRules() {
        return posXButtonRules;
    }

    public int getPosYButtonRules() {
        return posYButtonRules;
    }

    public int getPosXButtonQuit() {
        return posXButtonQuit;
    }

    public int getPosYButtonQuit() {
        return posYButtonQuit;
    }







}
