package model.deck;

public class CarteDeplacement extends Cartes{
    public CarteDeplacement() {

    }

    public String toString() {
        return "Je suis une carte de déplacement";
    }

    @Override
    public boolean isFrapper() {
        return false;
    }

    @Override
    public boolean isDeplacement() {
        return true;
    }

    @Override
    public boolean isMonter() {
        return false;
    }

    @Override
    public boolean isSheriff() {
        return false;
    }

    @Override
    public boolean isRamasser() {
        return false;
    }


}
