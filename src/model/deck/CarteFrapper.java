package model.deck;

public class CarteFrapper extends Cartes {
    public CarteFrapper() {
    }

    public String toString() {
        return "Je suis une carte frapper";
    }

    @Override
    public boolean isFrapper() {
        return true;
    }

    @Override
    public boolean isDeplacement() {
        return false;
    }

    @Override
    public boolean isMonter() {
        return false;
    }

    @Override
    public boolean isSheriff() {
        return false;
    }

    @Override
    public boolean isRamasser() {
        return false;
    }
}
