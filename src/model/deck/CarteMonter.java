package model.deck;

public class CarteMonter extends Cartes {
    public CarteMonter() {
    }

    public String toString() {
        return "Je suis une carte monter";
    }

    @Override
    public boolean isFrapper() {
        return false;
    }

    @Override
    public boolean isDeplacement() {
        return false;
    }

    @Override
    public boolean isMonter() {
        return true;
    }

    @Override
    public boolean isSheriff() {
        return false;
    }

    @Override
    public boolean isRamasser() {
        return false;
    }
}
