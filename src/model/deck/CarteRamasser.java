package model.deck;

public class CarteRamasser extends Cartes{
    public CarteRamasser() {
    }

    public String toString() {
        return "Je suis une carte ramasser";
    }

    @Override
    public boolean isFrapper() {
        return false;
    }

    @Override
    public boolean isDeplacement() {
        return false;
    }

    @Override
    public boolean isMonter() {
        return false;
    }

    @Override
    public boolean isSheriff() {
        return false;
    }

    @Override
    public boolean isRamasser() {
        return true;
    }
}
