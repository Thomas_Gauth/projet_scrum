package model.deck;

public class CarteSheriff extends Cartes {
    public CarteSheriff() {
    }

    public String toString() {
        return "Je suis une carte shérif";
    }

    @Override
    public boolean isFrapper() {
        return false;
    }

    @Override
    public boolean isDeplacement() {
        return false;
    }

    @Override
    public boolean isMonter() {
        return false;
    }

    @Override
    public boolean isSheriff() {
        return true;
    }

    @Override
    public boolean isRamasser() {
        return false;
    }
}
