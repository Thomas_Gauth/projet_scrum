package model.deck;

public abstract class Cartes {

    public Cartes() {

    }

    public String toString() {
        return "Je suis une carte";
    }

    public abstract boolean isFrapper();
    public abstract boolean isDeplacement();
    public abstract boolean isMonter();
    public abstract boolean isSheriff();
    public abstract boolean isRamasser();
}
