package model.objets;

public class Bijou extends Butin {

    public Bijou() {
        valeur = 500;
    }

    @Override
    public boolean isBijou() {
        return true;
    }

    @Override
    public boolean isMalette() {
        return false;
    }

    @Override
    public boolean isBourse() {
        return false;
    }
}
