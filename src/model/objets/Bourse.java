package model.objets;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Bourse extends Butin {
    private List<Integer> valeursPossibles;
    private Random random;

    public Bourse(int valeur) {
        super(valeur);
    }

    public Bourse() {
        valeursPossibles = new ArrayList<Integer>(5);
        valeursPossibles.add(250);
        valeursPossibles.add(300);
        valeursPossibles.add(350);
        valeursPossibles.add(400);
        valeursPossibles.add(450);
        random = new Random();
        valeur = valeursPossibles.get(random.nextInt(valeursPossibles.size()));
    }

    @Override
    public boolean isBijou() {
        return false;
    }

    @Override
    public boolean isMalette() {
        return false;
    }

    @Override
    public boolean isBourse() {
        return true;
    }
}
