package model.objets;

public abstract class Butin {
    protected int valeur;

    public Butin(){
        this.valeur = 250;
    }

    public Butin(int valeur) {
        this.valeur = valeur;
    }

    public int getValeur() {
        return valeur;
    }

    public abstract boolean isBijou();
    public abstract boolean isMalette();
    public abstract boolean isBourse();
}