package model.objets;

public class Malette extends Butin {

    public Malette() {
        valeur = 1000;
    }

    @Override
    public boolean isBijou() {
        return false;
    }

    @Override
    public boolean isMalette() {
        return true;
    }

    @Override
    public boolean isBourse() {
        return false;
    }
}
