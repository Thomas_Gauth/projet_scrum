package model.personnages;

import model.Constantes;
import model.deck.*;
import model.objets.Butin;
import model.objets.Bourse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import static java.util.Collections.shuffle;

public class Joueur extends Personnages implements Constantes{
    private List <Butin> tresor;
    public List <Cartes> deck;

    public List <Cartes> cartesEnMain;
    private Random random;
    private int nbBourse, nbMalette, nbBijou;

    public Joueur(int numeroJoueur) {
        super(numeroJoueur);
        random = new Random();
        tresor = new ArrayList<Butin>();
        tresor.add(new Bourse(250));
        nbBourse = 1;
        nbBijou = nbMalette = 0;
        deck = new ArrayList<Cartes>();
        cartesEnMain = new ArrayList<Cartes>();
    }

    public void initCartes() {
        deck.add(new CarteDeplacement());
        deck.add(new CarteDeplacement());
        deck.add(new CarteFrapper());
        deck.add(new CarteFrapper());
        deck.add(new CarteSheriff());
        deck.add(new CarteSheriff());
        deck.add(new CarteMonter());
        deck.add(new CarteMonter());
        deck.add(new CarteRamasser());
        deck.add(new CarteRamasser());
   //     piocheMain();
    }

    public void pioche() {
        cartesEnMain.add(deck.remove(random.nextInt(deck.size())));
    }

    public void piocheMain() {
        // PENSER A ENLEVER DU DECK LES CARTES PIOCHÉ
        Collections.shuffle(deck);
        for (int i=0; i<6; i++) {
            cartesEnMain.add(deck.get(i));
        }
        for (int i=0; i<6; i++) {
            System.out.println(cartesEnMain.get(i));
        }
        for (int i=0; i<6;i++)
        {
            // deck.remove(deck.get(0));
        }
        System.out.println();
    }

    public void retrieSesCartes() {
        for (int i = cartesEnMain.size(); i > 0; i--) {
            deck.add(cartesEnMain.remove(random.nextInt(cartesEnMain.size())));
        }
        for (int i = 0; i < NB_CARTES_EN_MAIN; i ++) {
            pioche();
        }
    }

    public List<Butin> getTresor() {
        return tresor;
    }

    public Butin perdUnButin(int numeroButin) {
        return tresor.remove(numeroButin);
    }

    public Butin perdUnButin() {
        int numeroButin = random.nextInt(this.tresor.size());
        return this.perdUnButin(numeroButin);
    }

    public void prendUnButin(Butin butin) {
        tresor.add(butin);
    }

    public void frappe(Joueur j, int numeroButin) {
        prendUnButin(j.perdUnButin(numeroButin));
    }


    public int getValeurTresor() {
      int somme = 0;
        for (Butin butin : tresor) {
            somme += butin.getValeur();
        }
      return somme;
    }



    public List<Cartes> getCartesEnMain() {
        return cartesEnMain;
    }

}
