package model.personnages;

import model.Constantes;

import java.awt.*;

public abstract class Personnages {
    public int numeroPersonnage;
    protected int numeroWagon;

    protected Color couleur;

    public Personnages(int numeroPersonnage) {
        this.numeroPersonnage = numeroPersonnage;
        switch (numeroPersonnage) {
            case 0:
                couleur = Constantes.COULEUR_JOUEUR1;
                break;
            case 1 :
                couleur = Constantes.COULEUR_JOUEUR2;
                break;
            case 2 :
                couleur = Constantes.COULEUR_JOUEUR3;
                break;
            case 3 :
                couleur = Constantes.COULEUR_JOUEUR4;
                break;
        }
        numeroWagon = 4-numeroPersonnage;
    }

    public void seDeplace(int deplacement) {
        numeroWagon += deplacement;
    }

    public Color getCouleur() {
        return couleur;
    }

    public int getNumeroPersonnage() {
        return numeroPersonnage;
    }

    public int getNumeroWagon() {
        return numeroWagon;
    }

    public void setNumeroWagon(int numeroWagon) {
        this.numeroWagon = numeroWagon;
    }
}
