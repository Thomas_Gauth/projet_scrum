package model.train;

import model.Constantes;

import java.util.ArrayList;
import java.util.List;

public class Train implements Constantes{
    public List<Wagon> wagons;

    public Train(int nbJoueur) {
        wagons = new ArrayList<Wagon>(nbJoueur+1);
        for(int i=0; i<= NB_JOUEURS; i++) {
            wagons.add(new Wagon(i));
        }
    }
}
