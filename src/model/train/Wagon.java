package model.train;
import model.objets.*;
import model.personnages.Joueur;
import model.personnages.Personnages;

import java.util.ArrayList;
import java.util.List;

public class Wagon {
    private List<Butin> butin;
    private List<Joueur> haut;
    private List<Personnages> bas;
    public int numeroWagon;
    private int nbBourse, nbMalette, nbBijou;


    public Wagon(int numWagon) {
        numeroWagon = numWagon;
        nbBijou = nbBourse = nbMalette = 0;
        butin = new ArrayList<Butin>();
        haut = new ArrayList<Joueur>();
        bas = new ArrayList<Personnages>();

        creerButin();
    }

    private void creerButin() {
        switch (numeroWagon){
            case 0:
                butin.add(new Malette());
                nbMalette = 1;

            case 1:
                butin.add(new Bourse());
                butin.add(new Bourse());
                butin.add(new Bijou());
                nbBourse = 2;
                nbBijou = 1;
                break;

            case 2:
                butin.add(new Bourse());
                butin.add(new Bourse());
                butin.add(new Bourse());
                butin.add(new Bijou());
                nbBourse = 3;
                nbBijou = 1;
                break;

            case 3:
                butin.add(new Bourse());
                butin.add(new Bourse());
                butin.add(new Bijou());
                butin.add(new Bijou());
                nbBourse = 2;
                nbBijou = 2;
                break;

            case 4:
                butin.add(new Bourse());
                butin.add(new Bourse());
                butin.add(new Bourse());
                butin.add(new Bourse());
                nbBourse = 4;
                break;

            case 5:
                butin.add(new Bourse());
                butin.add(new Bourse());
                butin.add(new Bourse());
                butin.add(new Bourse());
                butin.add(new Bijou());
                nbBourse = 4;
                nbBijou = 1;
                break;

            case 6:
                butin.add(new Bourse());
                butin.add(new Bourse());
                butin.add(new Bourse());
                butin.add(new Bourse());
                butin.add(new Bourse());
                nbBourse = 5;
                break;
        }
    }

    public int getNumeroWagon() {
        return numeroWagon;
    }

    public void setNumeroWagon(int numeroWagon) {
        this.numeroWagon = numeroWagon;
    }

    public List<Butin> getButin() {
        return butin;
    }

    public void setButin(List<Butin> butin) {
        this.butin = butin;
    }

    public List<Joueur> getHaut() {
        return haut;
    }

    public void setHaut(List<Joueur> haut) {
        this.haut = haut;
    }

    public List<Personnages> getBas() {
        return bas;
    }

    public void setBas(List<Personnages> bas) {
        this.bas = bas;
    }
}
