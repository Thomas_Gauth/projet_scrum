package view;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import model.MainModel;
import model.PanelModel;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;


public class Board extends JFrame {
	protected MainModel model;

	protected JPanel panoMain;

	protected Image imgIcon;

	protected PanoRailway panoRailway;
	protected PanoActions panoActions;
	protected PanoDeck panoDeck;

	protected PanoPlayerSet panoPlayerSet;

	protected JButton buttonDeck;
	protected String music = "sounds/django.wav";


	public Board(MainModel model){
		this.model = model;
		initAttributs();
    	addComponents();
    	setVisible(false);
	}

	public void initAttributs() {
		try {
			imgIcon = ImageIO.read(new File("img/logo.png"));
		} catch (IOException e) { e.printStackTrace(); }

		setResizable(false);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setSize(model.getPanel().getWidthBoard(),model.getPanel().getHeightBoard());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		setUndecorated(true);
		setIconImage(imgIcon);
	}

	public void addComponents(){
		this.panoMain = new JPanel();
		this.panoMain.setLayout(null);

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;

		panoRailway = new PanoRailway(model);
		panoPlayerSet = new PanoPlayerSet(this.model);
		panoDeck = new PanoDeck(this.model.getPanel());
		panoActions = new PanoActions(this.model);

		panoMain.add(panoRailway);
		panoMain.add(panoDeck);
		panoMain.add(panoPlayerSet);
		//panoMain.add(panoActions);

		setContentPane(panoMain);
	}

	public void switchPano(){
		if (panoActions.getParent() == panoMain){
			System.out.println("panoAction");
			panoMain.remove(panoActions);
			panoMain.add(panoPlayerSet);
		}
		else {
			System.out.println("panotruc");

			panoMain.remove(panoPlayerSet);
			panoMain.add(panoActions);
		}
		setContentPane(panoMain);



	}

	public void changePanoToAction() {
		panoMain.remove(panoPlayerSet);
		panoMain.add(panoActions);
		repaint();
	}

	public PanoActions getPanoActions() {
		return panoActions;
	}

	public void setPanoActions(PanoActions panoActions) {
		this.panoActions = panoActions;
	}

	public void afficher() {
		setVisible(true);
	}

    public PanoDeck getPanoDeck() {
        return panoDeck;
    }

    public void setPanoDeck(PanoDeck panoDeck) {
        this.panoDeck = panoDeck;
    }

    public PanoRailway getPanoRailway() {
        return panoRailway;
    }

    public void setPanoRailway(PanoRailway panoRailway) {
        this.panoRailway = panoRailway;
    }

	public PanoPlayerSet getPanoPlayerSet() {
		return panoPlayerSet;
	}
}
