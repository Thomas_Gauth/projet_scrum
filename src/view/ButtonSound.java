package view;

import controller.ControlGame;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by pelomedusa on 16/12/15.
 */
public class ButtonSound extends JButton {
    boolean audible;

    public ButtonSound(){
        super();
        audible = true;
        setSize(40,40);
        setIcon(new ImageIcon("img/bouton_son_on.png"));
        setBounds(910,10,40,40);
        setOpaque(false);
        setContentAreaFilled(false);
        setBorderPainted(false);

    }


    public boolean isAudible() {
        return audible;
    }

    public void setAudible(boolean audible) {
        this.audible = audible;
    }
}
