package view;

import model.MainModel;

public class MainWindow {
    private final Board board;
    private final Menu menu;

    public MainWindow(MainModel model) {
        board = new Board(model);
        menu = new Menu(model);
    }

    public Menu getMenu() {
        return menu;
    }

    public Board getBoard() {
        return board;
    }
}
