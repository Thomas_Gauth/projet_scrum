package view;
// Image2
// Chargement d'images en Java
// --------------------------------------------------

import controller.ControlMenu;
import model.MainModel;
import model.PanelModel;

import java.awt.*;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;

public class Menu extends JFrame {
    protected MainModel m;

    protected JPanel panel;
	protected JButton jBNouvellePartie;
	protected JButton jBAfficherRegles;
    protected JButton jBQuitter;
	protected Image imgFond;
    protected Image imgIcon;
    protected Image imgButtonPlay;
    protected Image imgButtonRules;
    protected Image imgButtonQuitter;


	// --------------------------------------------------
	// CREATION DU MENU
	// --------------------------------------------------
	public Menu(MainModel model) {
		super();
        m = model;
        initImages();
        initWindows();
		initAttributs();
        creerVue();
        setVisible(true);
     //   repaint(); // néccessaire car la fonction paint() implementé va afficher les images en premier, et les JButton ensuite
	}

    public void initImages() {
        try {
            imgFond = ImageIO.read(new File("img/menuBackground.jpg"));
            imgIcon = ImageIO.read(new File("img/logo.png"));
            imgButtonPlay = ImageIO.read(new File("img/bouton_jouer.png"));
            imgButtonRules = ImageIO.read(new File("img/bouton_regle.png"));
            imgButtonQuitter = ImageIO.read(new File("img/bouton_quitter.png"));
        } catch (IOException e) { e.printStackTrace(); }
    }


    public void initWindows() {
        setSize(m.getPanel().getWidthMenu(),
                m.getPanel().getHeightMenu());
        setUndecorated(true);
        setLayout(null);
        setIconImage(imgIcon);
        setTitle("Colt Express");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);
        setUndecorated(true);
    }

	// --------------------------------------------------
	// INITIALISATION DE L'APPLET
	// --------------------------------------------------

	public void initAttributs() {
        panel = new JPanel(){
            @Override
            public void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(imgFond, 0, 0, this);
            }
        };
		jBNouvellePartie = new JButton(new ImageIcon(imgButtonPlay));
		jBAfficherRegles = new JButton(new ImageIcon(imgButtonRules));
        jBQuitter = new JButton(new ImageIcon(imgButtonQuitter));
        panel.setLayout(null);

        jBNouvellePartie.setOpaque(false);
        jBNouvellePartie.setContentAreaFilled(false);
        jBNouvellePartie.setBorderPainted(false);
        jBNouvellePartie.setFocusPainted(false);
        jBNouvellePartie.setBounds(m.getPanel().getPosXButtonPlay(),
                                   m.getPanel().getPosYButtonPlay(),
                                   m.getPanel().getWidthButton(),
                                   m.getPanel().getHeightButton()
                                   );

        jBAfficherRegles.setOpaque(false);
        jBAfficherRegles.setContentAreaFilled(false);
        jBAfficherRegles.setBorderPainted(false);
        jBAfficherRegles.setFocusPainted(false);
        jBAfficherRegles.setBounds(m.getPanel().getPosXButtonRules(),
                m.getPanel().getPosYButtonRules(),
                m.getPanel().getWidthButton(),
                m.getPanel().getHeightButton()
        );

        jBQuitter.setOpaque(false);
        jBQuitter.setContentAreaFilled(false);
        jBQuitter.setBorderPainted(false);
        jBQuitter.setFocusPainted(false);
        jBQuitter.setBounds(m.getPanel().getPosXButtonQuit(),
                m.getPanel().getPosYButtonQuit(),
                m.getPanel().getWidthButton(),
                m.getPanel().getHeightButton()
        );

	}

	public void creerVue() {
		panel.add(jBNouvellePartie);
		panel.add(jBAfficherRegles);
        panel.add(jBQuitter);
        panel.setSize(m.getPanel().getWidthMenu(), m.getPanel().getHeightMenu());


		setContentPane(panel);
	}

	public void setControlMenu(ControlMenu c) {
		jBNouvellePartie.addActionListener(c);
		jBAfficherRegles.addActionListener(c);
        jBQuitter.addActionListener(c);
	}



    public MainModel getM() {
        return m;
    }

    public void setM(MainModel m) {
        this.m = m;
    }

    public JPanel getPanel() {
        return panel;
    }

    public void setPanel(JPanel panel) {
        this.panel = panel;
    }

    public JButton getjBNouvellePartie() {
        return jBNouvellePartie;
    }

    public void setjBNouvellePartie(JButton jBNouvellePartie) {
        this.jBNouvellePartie = jBNouvellePartie;
    }

    public JButton getjBAfficherRegles() {
        return jBAfficherRegles;
    }

    public void setjBAfficherRegles(JButton jBAfficherRegles) {
        this.jBAfficherRegles = jBAfficherRegles;
    }

    public JButton getjBQuitter() {
        return jBQuitter;
    }

    public void setjBQuitter(JButton jBQuitter) {
        this.jBQuitter = jBQuitter;
    }

    public Image getImgFond() {
        return imgFond;
    }

    public void setImgFond(Image imgFond) {
        this.imgFond = imgFond;
    }

    public Image getImgIcon() {
        return imgIcon;
    }

    public void setImgIcon(Image imgIcon) {
        this.imgIcon = imgIcon;
    }

    public Image getImgButtonPlay() {
        return imgButtonPlay;
    }

    public void setImgButtonPlay(Image imgButtonPlay) {
        this.imgButtonPlay = imgButtonPlay;
    }

    public Image getImgButtonRules() {
        return imgButtonRules;
    }

    public void setImgButtonRules(Image imgButtonRules) {
        this.imgButtonRules = imgButtonRules;
    }

    public Image getImgButtonQuitter() {
        return imgButtonQuitter;
    }

    public void setImgButtonQuitter(Image imgButtonQuitter) {
        this.imgButtonQuitter = imgButtonQuitter;
    }
}
