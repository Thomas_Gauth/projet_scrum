package view;

import model.MainModel;
import view.deck.SlotCarte;
import view.objets.Bijou;
import view.objets.Bourse;
import view.objets.Malette;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.CompoundBorder;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by pelomedusa on 17/12/15.
 */
public class PanoActions extends JPanel {

    private BufferedImage imgBackground;
    private MainModel m;

    private JButton flecheDroiteDeplacer;
    private JButton flecheGaucheDeplacer;

    private JButton boutonEchelle;

    private JButton[] personnagesFrapper;

    private Bijou bijou;
    private Malette malette;
    private Bourse bourse;



    public PanoActions(MainModel m){
        this.m = m;
        setBounds(0, 300, 720,200);

        try {
            imgBackground = ImageIO.read(new File("img/playersetBackground.png"));
        } catch (IOException e) {
            System.out.println("background playerset n'existe pas");
            e.printStackTrace();
        }

        setLayout(null);
        addFleches();
        ajouterBoutonsFrapper();
        addBoutonsButin();
    }

    private void addBoutonsButin() {
        bijou = new Bijou();
        malette = new Malette();
        bourse = new Bourse();

        this.add(bijou);
        this.add(malette);
        this.add(bourse);

    }

    private void addFleches() {
        flecheGaucheDeplacer = new JButton(new ImageIcon("img/flechegauche.png"));
        flecheDroiteDeplacer = new JButton(new ImageIcon("img/flechedroite.png"));
        boutonEchelle = new JButton(new ImageIcon("img/echelle.png"));

        flecheGaucheDeplacer.setRolloverIcon(new ImageIcon("img/flechegauche_hover.png"));
        flecheDroiteDeplacer.setRolloverIcon(new ImageIcon("img/flechedroite_hover.png"));
        boutonEchelle.setRolloverIcon(new ImageIcon("img/echelle_hover.png"));

        flecheGaucheDeplacer.setSize(100,50);
        flecheDroiteDeplacer.setSize(100,50);
        boutonEchelle.setSize(50,100);

        flecheGaucheDeplacer.setOpaque(false);flecheGaucheDeplacer.setContentAreaFilled(false);flecheGaucheDeplacer.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        flecheDroiteDeplacer.setOpaque(false);flecheDroiteDeplacer.setContentAreaFilled(false);flecheDroiteDeplacer.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        boutonEchelle.setOpaque(false);boutonEchelle.setContentAreaFilled(false);boutonEchelle.setBorder(BorderFactory.createLineBorder(Color.black, 1));

        flecheGaucheDeplacer.setBounds(50,75,100,50);
        flecheDroiteDeplacer.setBounds(250,75,100,50);
        boutonEchelle.setBounds(175,50,50,100);

        //flecheDroiteDeplacer.setVisible(false);
        //flecheGaucheDeplacer.setVisible(false);


        this.add(flecheDroiteDeplacer);
        this.add(flecheGaucheDeplacer);
        this.add(boutonEchelle);

    }

    public void ajouterBoutonsFrapper(){
        personnagesFrapper = new JButton[4];

        for (int i=0;i<4;i++){
            personnagesFrapper[i] = new JButton(new ImageIcon("img/frapper.png"));
            System.out.println(m.getGame().getJoueurs().get(i).getCouleur());
            personnagesFrapper[i].setBackground(m.getGame().getJoueurs().get(i).getCouleur());
            //System.out.println(m.getGame().getJoueurs().get(i).getNumeroPersonnage());
            personnagesFrapper[i].setRolloverIcon(new ImageIcon("img/frapper_hover.png"));
            //personnagesFrapper[i].setSelectedIcon(new ImageIcon("img/frapper.png"));
            personnagesFrapper[i].setSize(50,50);

            if (i==0){
                personnagesFrapper[i].setBounds(500,50,50,50);
            }
            else if (i==1){
                personnagesFrapper[i].setBounds(550,50,50,50);
            }
            else if (i==2){
                personnagesFrapper[i].setBounds(500,100,50,50);
            }
            else if (i==3){
                personnagesFrapper[i].setBounds(550,100,50,50);
            }
            this.add(personnagesFrapper[i]);
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        // Dessin de l'image de fond
        super.paintComponent(g);

        g.drawImage(imgBackground, 0, 0, this);
    }

    public BufferedImage getImgBackground() {
        return imgBackground;
    }

    public void setImgBackground(BufferedImage imgBackground) {
        this.imgBackground = imgBackground;
    }

    public MainModel getM() {
        return m;
    }

    public void setM(MainModel m) {
        this.m = m;
    }

    public JButton getFlecheDroiteDeplacer() {
        return flecheDroiteDeplacer;
    }

    public void setFlecheDroiteDeplacer(JButton flecheDroiteDeplacer) {
        this.flecheDroiteDeplacer = flecheDroiteDeplacer;
    }

    public JButton getFlecheGaucheDeplacer() {
        return flecheGaucheDeplacer;
    }

    public void setFlecheGaucheDeplacer(JButton flecheGaucheDeplacer) {
        this.flecheGaucheDeplacer = flecheGaucheDeplacer;
    }

    public JButton getBoutonEchelle() {
        return boutonEchelle;
    }

    public void setBoutonEchelle(JButton boutonEchelle) {
        this.boutonEchelle = boutonEchelle;
    }

    public JButton[] getPersonnagesFrapper() {
        return personnagesFrapper;
    }

    public void setPersonnagesFrapper(JButton[] personnagesFrapper) {
        this.personnagesFrapper = personnagesFrapper;
    }

    public Bijou getBijou() {return bijou;}

    public void setBijou(Bijou bijou) {this.bijou = bijou;}

    public Malette getMalette() {return malette;}

    public void setMalette(Malette malette) {this.malette = malette;}

    public Bourse getBourse() {return bourse;}

    public void setBourse(Bourse bourse) {this.bourse = bourse;}
}
