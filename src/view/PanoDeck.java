package view;

import model.PanelModel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Rectangle;
import java.awt.TexturePaint;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.LineBorder;


public class PanoDeck extends JPanel{
	
	private PanelModel m;
	private JButton buttonDeck;
	private GridBagConstraints gbc;
	private JButton panoCarte;
	
	public PanoDeck(PanelModel m){
		super(new GridBagLayout());
		this.setBackground(Color.green);
		this.setBorder(new LineBorder(Color.BLACK, 2));
		this.setBounds(720,300, 240,200);
		//this.setPreferredSize(new Dimension(m.getWidthDeck(),m.getHeightDeck()));
		
		this.buttonDeck = new JButton();
		//this.buttonDeck.setPreferredSize(new Dimension(200,267));
		this.buttonDeck.setPreferredSize(new Dimension(100,133));
		
		this.buttonDeck.setIcon(new ImageIcon("img/carte.png"));
		this.buttonDeck.setPressedIcon(new ImageIcon("img/carte-sombre.png"));
		this.buttonDeck.setRolloverIcon(new ImageIcon("img/carte-sombre.png"));
		
		this.gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weightx =1;
		gbc.weighty = 1;
        //gbc.anchor = GridBagConstraints.CENTER;
        //gbc.fill = GridBagConstraints.NONE;
		this.add(this.buttonDeck,gbc);
		
		this.panoCarte = new JButton();
		//this.panoCarte.setPreferredSize(new Dimension(200,267));
		this.panoCarte.setPreferredSize(new Dimension(100,133));

		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.weightx =1;
		gbc.weighty = 1;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.fill = GridBagConstraints.NONE;
		this.add(this.panoCarte,gbc);
	}

	
	@Override
    protected void paintComponent(Graphics g) {
		BufferedImage bi;
		try {
			bi = ImageIO.read(new File("img/Sand_texture2.png"));
			TexturePaint paint = new TexturePaint(bi, new Rectangle(0, 0, bi.getWidth(), bi.getHeight()));
	        Graphics2D g2 = (Graphics2D) g;
	        g2.setPaint(paint);
	        g2.fill(new Rectangle(0, 0, getWidth(), getHeight()));
		} catch (IOException e) {
			e.printStackTrace();
		}
    }


	public JButton getButtonDeck() {
		return buttonDeck;
	}

	public void setButtonDeck(JButton buttonDeck) {
		this.buttonDeck = buttonDeck;
	}

	public JButton getPanoCarte() {
		return panoCarte;
	}

	public void setPanoCarte(JButton panoCarte) {
		this.panoCarte = panoCarte;
	}
}
