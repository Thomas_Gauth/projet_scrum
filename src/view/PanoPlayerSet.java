package view;

/**
 * Created by pelomedusa on 15/12/15.
 */
import model.MainModel;
import model.PanelModel;
import view.deck.SlotCarte;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class PanoPlayerSet extends JPanel {

    private BufferedImage imgBackground;
    private SlotCarte[] slots;
    private MainModel m;

    public PanoPlayerSet(MainModel m){
        this.m = m;
        setBounds(0, 300, 720,200);

        try {
            imgBackground = ImageIO.read(new File("img/playersetBackground.png"));
        } catch (IOException e) {
            System.out.println("background playerset n'existe pas");
            e.printStackTrace();
        }

        setLayout(null);
        addSlots();
    }

    private void addSlots() {
        slots = new SlotCarte[5];
        for (int i=0;i<5;i++) {
            slots[i] = new SlotCarte(i);
            add(slots[i]);
            System.out.println("done "+i);
        }
        System.out.println("over");

    }

    public void changeCarteColor(Color lastColor, Color couleurActuel) {
        for (int i=0;i<5;i++) {
            slots[i].getCarte().changeColor(slots[i].getCarte().getB(), lastColor, couleurActuel);
        }
        repaint();
    }

    @Override
    public void paintComponent(Graphics g) {
        // Dessin de l'image de fond
        super.paintComponent(g);

        g.drawImage(imgBackground, 0, 0, this);
    }

    public SlotCarte[] getSlots() {
        return slots;
    }

}
