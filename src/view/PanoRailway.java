package view;

import model.MainModel;
import model.PanelModel;
import model.personnages.Joueur;
import model.train.Train;

import java.awt.*;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.LineBorder;


public class PanoRailway extends JPanel{

	private Train train;
	private ButtonSound bs;
	private JLabel jlNbTour;
	MainModel m;

	JButton flecheDroiteDeplacer;
	JButton flecheGaucheDeplacer;
	Wagon[] wagons;


	public PanoRailway(MainModel m){
		super();
		this.m = m;
		setLayout(null);
		this.setBorder(new LineBorder(Color.BLACK, 2));
		setBounds(0, 0, 960, 300);

		jlNbTour = new JLabel("Numéro tour : " + m.getGame().numeroTour);
		jlNbTour.setFont(new Font("Arial", Font.BOLD, 24));
		jlNbTour.setBounds(650, 10, 300, 50);
		add(jlNbTour);

		// BOUTON SON ON/OFF
		bs = new ButtonSound();
		this.add(bs);

		wagons = new Wagon[5];
		//AJOUT WAGONS
		for (int i=0;i<5;i++){
			wagons[i] = new view.Wagon(i);
			this.add(wagons[i]);
		}
		//repaint();


	}

	public void setNumeroTour() {
		jlNbTour.setText("Numéro tour : " + m.getGame().numeroTour);
	}


	@Override
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		try {
			g.drawImage(ImageIO.read(new File("img/canyon.png")),0,0,null);
		}catch (IOException e){System.out.println("Image canyon.jpg non trouvée.");System.exit(0);}
	}


	public JButton getFlecheGaucheDeplacer() {
		return flecheGaucheDeplacer;
	}

	public void setFlecheGaucheDeplacer(JButton flecheGaucheDeplacer) {
		this.flecheGaucheDeplacer = flecheGaucheDeplacer;
	}

	public JButton getFlecheDroiteDeplacer() {
		return flecheDroiteDeplacer;
	}

	public void setFlecheDroiteDeplacer(JButton flecheDroiteDeplacer) {
		this.flecheDroiteDeplacer = flecheDroiteDeplacer;
	}

	public ButtonSound getBs() {
		return bs;
	}

	public void setBs(ButtonSound bs) {
		this.bs = bs;
	}

	public Wagon[] getWagons() {
		return wagons;
	}

	public void setWagons(Wagon[] wagons) {
		this.wagons = wagons;
	}
}
