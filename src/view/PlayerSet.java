package view;

import model.MainModel;
import model.PanelModel;
import view.deck.CarteSheriff;
import view.deck.SlotCarte;
import view.slotJoueur;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by ajossic on 13/11/15.
 */
public class PlayerSet extends JFrame{
    private BufferedImage imgBackground;
    private SlotCarte[] slots;
    private JPanel panoMain;
    private MainModel m;

    public PlayerSet(MainModel m) {
        super();
        this.m=m;
        setSize(600,200);
        setLayout(null);
        setUndecorated(true);


        setTitle("Colt Express");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocation((int) Toolkit.getDefaultToolkit().getScreenSize().getWidth()/2-300,(int) Toolkit.getDefaultToolkit().getScreenSize().getHeight()-300 );

        try {
            imgBackground = ImageIO.read(new File("img/playersetBackground.png"));
        } catch (IOException e) {
            System.out.println("background playerset n'existe pas");
            e.printStackTrace();
        }

        panoMain = new JPanel(){
            @Override
            public void paintComponent(Graphics g) {
                // Dessin de l'image de fond
                super.paintComponent(g);

                g.drawImage(imgBackground, 0, 0, this);
            }
        };
        panoMain.setLayout(null);

        addSlots();
        setContentPane(panoMain);
        setVisible(true);


    }

    private void addSlots() {
        slots = new SlotCarte[6];
        for (int i=0;i<6;i++) {
            slots[i] = new SlotCarte(i);
            panoMain.add(slots[i]);
            System.out.println("done "+i);
        }
        System.out.println("over");

    }





}
