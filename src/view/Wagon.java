package view;

import model.personnages.Joueur;
import view.objets.Bijou;
import view.objets.Bourse;
import view.objets.Malette;
import view.slotJoueur;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * Created by pelomedusa on 03/12/15.
 */
public class Wagon extends JPanel {
    model.train.Wagon mw;

    Bijou bijou;
    Bourse bourse;
    Malette malette;
    slotJoueur[] slots;

    public Wagon(int numeroWagon) {
        super();

        mw = new model.train.Wagon(numeroWagon);
        //setBackgroundImage();
        setPreferredSize(new Dimension(160,202));

        if (mw.getNumeroWagon() == 0){
            setBounds((numeroWagon * 218 + 5) + 5, 0, 218, 167);
        }else {
            setBounds(50 + (numeroWagon * 160 + 5) + 5, 0, 160, 167);
        }
        setLayout(null);
        this.setOpaque(false);
        ajouterSlotsJoueurs();
    }

    private void ajouterSlotsJoueurs() {
        if (mw.getNumeroWagon() != 0) {
            slots = new slotJoueur[9];
            for (int i=0;i<9;i++){
                slots[i] = new slotJoueur();
                switch (i){
                    case 0:
                        slots[i].setBounds(0, 90, 40, 50);
                        break;
                    case 1:
                        slots[i].setBounds(30, 90, 40, 50);
                        break;
                    case 2:
                        slots[i].setBounds(60, 90, 40, 50);
                        break;
                    case 3:
                        slots[i].setBounds(90, 90, 40, 50);
                        break;
                    case 4:
                        slots[i].setBounds(120, 90, 40, 50);
                        break;
                    case 5:
                        slots[i].setBounds(10, 40, 40, 50);
                        break;
                    case 6:
                        slots[i].setBounds(45, 40, 40, 50);
                        break;
                    case 7:
                        slots[i].setBounds(80, 40, 40, 50);
                        break;
                    case 8:
                        slots[i].setBounds(115, 40, 40, 50);
                        break;
                }
                add(slots[i]);
            }
        } else {
            slots = new slotJoueur[1];
            slots[0] = new slotJoueur();
            slots[0].setBounds(160, 80, 40, 50);
            add(slots[0]);
        }
    }

    private void afficherNbrObjet() {
        int nbrBourse =0;
        int nbrMalette =0;
        int nbrBijou = 0;
        for (int i=0;i<mw.getButin().size();i++){
            if (mw.getButin().get(i) instanceof model.objets.Bijou){
                nbrBijou++;
            }
            else if (mw.getButin().get(i) instanceof model.objets.Malette){
                nbrMalette++;
            }
            else if (mw.getButin().get(i) instanceof model.objets.Bourse){
                nbrBourse++;
            }
        }
    //    bourse.setText(Integer.toString(nbrBourse));
       // malette.setText(""+nbrMalette);
        bijou.setText(""+nbrBijou);
    }

    public boolean ajouterJoueur(Joueur j){
        for (int i=0;i<this.slots.length;i++){
            if (slots[i].getJ() == null) {
                slots[i].ajouterJoueur(j);
                return true;
            }
        }
        return false;
    }

    


    private void ajouterSlotsObjets() {
        malette = new Malette();
        bourse = new Bourse();
        bijou = new Bijou();
        this.add(malette);
        if (mw.getNumeroWagon()!=0) {
            this.add(bourse);
            this.add(bijou);
        }

    }

    @Override
    protected void paintComponent(Graphics g){
        super.paintComponent(g);
        try {
            if (mw.getNumeroWagon() == 0){
                g.drawImage(ImageIO.read(new File("img/locomotive.png")), 0, 65, null);
            } else {
                g.drawImage(ImageIO.read(new File("img/wagon.png")), 0, 65, null);
            }
        }
        catch (IOException e){System.out.println("Image wagon.png non trouvée.");System.exit(0);}
    }
}