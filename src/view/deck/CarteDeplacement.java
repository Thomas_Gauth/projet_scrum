package view.deck;

import java.awt.*;

/**
 * Created by pelomedusa on 13/12/15.
 */
public class CarteDeplacement extends Cartes{

    public CarteDeplacement(Color color) {
        super("img/iconeCarteDeplacement.png", color);
    }
}
