package view.deck;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by pelomedusa on 13/12/15.
 */
public class Cartes extends ImageIcon {

    private Color color;
    private BufferedImage b;

    public Cartes(String pathIcon, Color color) {
        super();
        try {
            b = ImageIO.read(new File(pathIcon));
            setImage(b);
            changeColor(b, color);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void changeColor(BufferedImage img, Color newColor) {
        final int newRGB = newColor.getRGB();
        for (int x = 0; x < img.getWidth(); x++) {
            for (int y = 0; y < img.getHeight(); y++) {
                if (img.getRGB(x, y) == Color.BLACK.getRGB()) {
                    img.setRGB(x, y, newRGB);
                }
            }
        }
    }

    public void changeColor(BufferedImage img, Color lastColor, Color newColor) {
        final int newRGB = newColor.getRGB();
        for (int x = 0; x < img.getWidth(); x++) {
            for (int y = 0; y < img.getHeight(); y++) {
                if (img.getRGB(x, y) == lastColor.getRGB()) {
                    img.setRGB(x, y, newRGB);
                }
            }
        }
    }

    public BufferedImage getB() {
        return b;
    }



}
