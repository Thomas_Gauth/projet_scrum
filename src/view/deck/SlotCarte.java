package view.deck;

import model.Constantes;

import javax.swing.*;
import java.awt.*;

/**
 * Created by pelomedusa on 13/12/15.
 */
public class SlotCarte extends JButton {

    private Cartes carte;
    private int nbCarte;
    private JLabel jlNbCarte;

    public SlotCarte(int numeroCarte){
        super();
        nbCarte = 0;
        setPreferredSize(new Dimension(90,150));
        setHorizontalTextPosition(JButton.CENTER);
        setVerticalTextPosition(JButton.CENTER);
        setFont(new Font("Copperplate Gothic", Font.BOLD, 42));
        setForeground(Color.decode("#800018"));
        switch (numeroCarte){
            case 0:
                setBounds(45, 25, 90,150);
                carte = new CarteSheriff(Constantes.COULEUR_JOUEUR1);
                break;
            case 1:
                setBounds(180, 25, 90,150);
                carte = new CarteRamasser(Constantes.COULEUR_JOUEUR1);
                break;
            case 2:
                setBounds(315, 25, 90,150);
                carte = new CarteMonter(Constantes.COULEUR_JOUEUR1);
                break;
            case 3:
                setBounds(450, 25, 90,150);
                carte = new CarteDeplacement(Constantes.COULEUR_JOUEUR1);
                break;
            case 4:
                setBounds(585, 25, 90,150);
                carte = new CarteFrapper(Constantes.COULEUR_JOUEUR1);
                break;
        }
        setIcon(carte);
        setEnabled(false);
     //   displayNbCarte();
    }

    public void displayNbCarte() {
        setText("" + nbCarte);
    }

    public void setNbCarte(int nb) {
        setText("" + nbCarte);
    }

    public Cartes getCarte() {
        return carte;
    }

}
