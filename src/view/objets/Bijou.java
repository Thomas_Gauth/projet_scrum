package view.objets;

import javax.swing.*;
import java.awt.*;

/**
 * Created by pelomedusa on 04/12/15.
 */
public class Bijou extends JButton {
    public Bijou(){
        super(""+1, new ImageIcon("img/bijou.png"));
        setHorizontalTextPosition(JButton.CENTER);
        setVerticalTextPosition(JButton.CENTER);
        setPreferredSize(new Dimension(50,50));
        setFont(new Font("Arial", Font.BOLD, 8));
        setOpaque(false);
        setContentAreaFilled(false);
        setBorderPainted(false);
        setBounds(650,13,50,50);

    }

}
