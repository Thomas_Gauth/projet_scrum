package view.objets;

import javax.swing.*;
import java.awt.*;

/**
 * Created by pelomedusa on 04/12/15.
 */
public class Bourse extends JButton {
    public Bourse(){
        super(""+1, new ImageIcon("img/bourse.png"));
        setHorizontalTextPosition(JButton.CENTER);
        setVerticalTextPosition(JButton.CENTER);
        setPreferredSize(new Dimension(50,50));
        setFont(new Font("Arial", Font.BOLD, 8));
        setOpaque(false);
        setContentAreaFilled(false);
        setBorderPainted(false);
        setBounds(650,76,50,50);
    }

}
