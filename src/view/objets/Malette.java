package view.objets;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * Created by pelomedusa on 04/12/15.
 */
public class Malette extends JButton {
    public Malette(){
        super(""+1, new ImageIcon("img/malette.png"));
        setHorizontalTextPosition(JButton.CENTER);
        setVerticalTextPosition(JButton.CENTER);
        setPreferredSize(new Dimension(50,50));
        setFont(new Font("Arial", Font.BOLD, 8));
        setOpaque(false);
        setContentAreaFilled(false);
        setBorderPainted(false);
        setBounds(650,139,50,50);
    }

}
