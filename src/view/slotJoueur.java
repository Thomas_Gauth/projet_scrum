package view;

import model.Constantes;
import model.personnages.Joueur;
import view.Personnage;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by pelomedusa on 04/12/15.
 */
public class slotJoueur extends JButton {
    Joueur j;
    public slotJoueur(){
        super(); //super("", new ImageIcon("img/silhouette.png"));
        setBackground(Color.blue);
        setHorizontalTextPosition(JButton.CENTER);
        setVerticalTextPosition(JButton.CENTER);
        setPreferredSize(new Dimension(30,40));
        setFont(new Font("Arial", Font.BOLD, 8));
        setOpaque(false);
        setContentAreaFilled(false);
        setBorderPainted(false);
    }

    public void ajouterJoueur(Joueur j){
        this.j = j;
        BufferedImage img = null;
        try {
            if (j.getCouleur() == Constantes.COULEUR_JOUEUR1) {
                img = ImageIO.read(new File("img/silhouetteBlue.png"));
            } else if (j.getCouleur() == Constantes.COULEUR_JOUEUR2) {
                img = ImageIO.read(new File("img/silhouetteRed.png"));
            } else if (j.getCouleur() == Constantes.COULEUR_JOUEUR3) {
                img = ImageIO.read(new File("img/silhouetteGreen.png"));
            } else {
                img = ImageIO.read(new File("img/silhouetteYellow.png"));
            }
           //changeColor(img);
            setIcon(new ImageIcon(img));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void changeColor(BufferedImage img) {
        final int newRGB = j.getCouleur().getRGB();
        for (int x = 0; x < img.getWidth(); x++) {
            for (int y = 0; y < img.getHeight(); y++) {
                System.out.println(img.getRGB(x, y)+"");
                if (img.getRGB(x, y) == Color.BLACK.getRGB()) {
                    img.setRGB(x, y, newRGB);
                }
            }
        }
    }

    public Joueur getJ() {
        return j;
    }

    public void setJ(Joueur j) {
        this.j = j;
    }
}
